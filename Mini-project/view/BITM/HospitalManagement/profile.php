<?php
	include_once("../../../vendor/autoload.php");
	
	use App\BITM\RegistrationForm\PatientInformation;
	use App\BITM\Handler\Utility;
	
	$info = new PatientInformation();
	$profile = $info->profile($_GET['id']);
	
	//Utility::dd($profile);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../../../resource/Bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- CSS -->
	<link href="../../../resource/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="main">
		<div id="header">
			<figure>
				<a href="index.html"><img src="../../../resource/images/logo.png" width="53" height="65" alt="logo"></a>
				<a href="index.html"><h1>Hospital Registration</h1></a>
			</figure>
			<nav class="nav">
				<ul>
					<li><a class="" href="index.html">Home</a></li>
					<li><a href="list.php">Patient list</a></li>
				</ul>
			</nav>
		</div>
		<div id="container">
			<div class="SubContainer">
			<div class="list-nav">
								<ul>
									<li>
									</li>
									<li><a href="">Download Excel</a></li>
									<li><a href="">Download PDF</a></li>
									<li><a href="index.html">Back to Profile</a></li>
								</ul>
							</div>
				<div id="jumbotron1" class="jumbotron">
					<div class="progile-container">
						<figure id="profile-picture">
							<!--<img src="../../../resource/images/profile-pic.jpg" width="200" height="200" alt="" class="img-thumbnail"/>-->
							
							<?php echo "<img src='images/$profile->profile_picture' width='200' height='200' alt='profile-picture' class='img-thumbnail';>"?><h4>ID:&nbsp;&nbsp;<?php echo $profile->id;?></h4>
						</figure>
						
						<div id="profile-title">
							<h4>Patient Profile</h4>
						</div>
						
						<div id="profile-name">
							<h4><?php echo $profile->first_name;?>&nbsp;<?php echo $profile->last_name;?></h4>
						</div>
						
						
						<div id="inner-nav">
							<ul>
								<li><a href="create_advice.php?id=<?php echo $profile->id;?>">
								<img src="../../../resource/images/advice.png" width="64" height="64" alt=""/>
								Give Advice</a></li>
								<li><a href="history_list.php?id=<?php echo $profile->id;?>"><img src="../../../resource/images/history.png" width="64" height="64" alt=""/>
								Medical History</a></li>
								<li><a href="create_bill.php?id=<?php echo $profile->id;?>"><img src="../../../resource/images/bill.png" width="64" height="64" alt=""/>
								Make Bill</a></li>
								<li><a href="bill_list.php?id=<?php echo $profile->id;?>"><img src="../../../resource/images/invoice.png" width="64" height="64" alt=""/>
								Bill list</a></li>
							</ul>
						</div>
						
						<table id="profile-table" border="0">
							<tr>
								<th><div class="column-a">Birthday:</div></th>
								<td><div class="column-b"><?php echo $profile->DOB;?></div></td>
								<th><div class="column-c">Address:</div></th>
								<td><div class="column-d"><address class="address"><?php echo $profile->Address;?></address></div></td>
								<th><div class="column-e">City:</div></th>
								<td><div class="column-f"><?php echo $profile->City;?></div></td>
							</tr>
							<tr>
								<th><div class="column-a">Sex:</div></th>
								<td><div class="column-b"><?php echo $profile->SEX;?></div></td>
								<th><div class="column-c">Phone:</div></th>
								<td><div class="column-d"><?php echo $profile->Phone;?></div></td>
								<th><div class="column-e">National ID Number:</div></th>
								<td><div class="column-f"><?php echo $profile->NID;?></div></td>
							</tr>
							<tr>
								<th><div class="column-a">Email Address:</div></th>
								<td><div class="column-b"><?php echo $profile->email_address;?></div></td>
								<th><div class="column-c">Food Habit:</div></th>
								<td><div class="column-d"><?php echo $profile->food_habit;?></div></td>
								<th><div class="column-e">Major Diseases:</div></th>
								<td><div class="column-f">
									<ul>
										<li><?php echo $profile->Diabetes;?></li>
										<li><?php echo $profile->Cholesterol;?></li>
										<li><?php echo $profile->Coronary;?>.</li>
										<li><?php echo $profile->Hypertension;?></li>
										<li><?php echo $profile->None;?></li>
									</ul>
								</div></td>
							</tr>
						</table>
						
						
						
						
						
						
						
						
					</div>
				</div>
			</div>
		</div>
		<div id="footer">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>