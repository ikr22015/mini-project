<?php
	session_start();
	include_once("../../../vendor/autoload.php");
	
	use App\BITM\RegistrationForm\PatientInformation;
	use App\BITM\Handler\Utility;
	
	
	$info = new PatientInformation();
	$allinfo = $info->list_of_data();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../../../resource/Bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- CSS -->
	<link href="../../../resource/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="main">
		<div id="header">
			<figure>
				<a href="index.html"><img src="../../../resource/images/logo.png" width="53" height="65" alt="logo"></a>
				<a href="index.html"><h1>Hospital Registration</h1></a>
			</figure>
			<nav class="nav">
				<ul>
					<li><a class="" href="index.html">Home</a></li>
					<li><a href="list.php">Patient list</a></li>
				</ul>
			</nav>
		</div>
		<div id="container">
			<div class="SubContainer">
				<div id="jumbotron" class="jumbotron">
					<div class="container">
						<div class="booklist-sub-container">
							<div class="list-nav">
								<ul>
									<li>
										<form action="filter.php" method="POST">
											<input type="number" name="filter" placeholder="Search by ID...">
											<button type="submit" name="" value="">Search</button>
										</form>
									</li>
									<li><a href="01simple-download-xlsx.php">Download Excel</a></li>
									<li><a href="pdf.php">Download PDF</a></li>
									<li><a href="index.html">New Registration</a></li>
								</ul>
							</div>
							
							<?php
							//var_dump($_SESSION);
							if(isset($_SESSION['message'])&&!empty($_SESSION['message'])){
								//echo "hello";
							$a=Utility::message();
						    echo $html=<<<EOD
							<div id="success-alert" class="alert alert-success aler-dismissable" role="alert">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
									<p> $a</p>
									
							</div><!--Successfull message-->
EOD;
							}
?>
							
							<div id="profile-sum-org-panel-success" class="panel panel-primary">
								<div id="profile-sum-org-panel-heading" class="panel-heading">
									<p style="margin: 0; text-align:center;">Patient list</p>
								</div>
								<table class="table table-bordered" border="1">
									<thead>
										<tr>
											<th style="text-align:center">SL</th>
											<th style="text-align:center">ID</th>
											<th style="text-align:center">First Name</th> 
											<th style="text-align:center">Last Name</th> 
											<th style="text-align:center">Action</th>
										</tr>
									</thead>
									<tbody>
									<?php
									$SL = 1;
									foreach($allinfo as $info){
									?>
										<tr>
											<td><?php echo $SL?></td>
											<td><?php echo $info->id?></td> 
											<td><?php echo $info->first_name?></td>  
											<td><?php echo $info->last_name?></td>  
											<td style="text-align:center">
												<button type="button" name="button" value=""><a href="profile.php?id=<?php echo $info->id?>">View</a></button>
												<button type="button" name="button" value=""><a href="edit.php?id=<?php echo $info->id?>">Edit</a></button>
												<input type="hidden" name="id" value="<?php echo $info->id?>">
												<button type="button" class="delete" name="button" value=""><a href="delete.php?id=<?php echo $info->id?>">Delete</a></button>
											</td>
										</tr>
									<?php
									$SL++;
									}
									?>	
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="footer">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../resource/Bootstrap/js/bootstrap.min.js"></script>
	
	<script>
		$('.delete').bind('click',function(e){
			var deleteItem = confirm("Hello Mr Imran");
			if(!deleteItem){
				e.preventDefault();
			}
		});
	</script>
  </body>
</html>