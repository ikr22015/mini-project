<?php
	session_start();
	include_once("../../../vendor/autoload.php");
	
	use App\BITM\RegistrationForm\PatientInformation;
	use App\BITM\Handler\Utility;
	
	
	$info = new PatientInformation();
	$allinfo = $info->list_of_data();
	$trs = "";
?>

									<?php
									$SL = 1;
									foreach($allinfo as $info){
										
										$trs .="<tr>";
										$trs .="<td>".$SL."</td>";
										$trs .="<td>".$info->id."</td>";
										$trs .="<td>".$info->first_name."</td>";
										$trs .="<td>".$info->last_name."</td>";
										$trs .="</tr>";
									$SL++;
									}
									?>
										
										
										
									


<?php
$html = <<<EOD
	<!DOCTYPE html>
<html lang="en">
  <head>
  </head>
  <body>
    <div id="main">
		<div id="container">
			<div class="SubContainer">
				<div id="jumbotron" class="jumbotron">
					<div class="container">
						<div class="booklist-sub-container">
							<div id="profile-sum-org-panel-success" class="panel panel-primary">
								<div id="profile-sum-org-panel-heading" class="panel-heading">
									<h1>Patient list</h1>
								</div>
								<table class="table table-bordered" border="1">
									<thead>
										<tr>
											<th style="text-align:center">SL</th>
											<th style="text-align:center">ID</th>
											<th style="text-align:center">First Name</th> 
											<th style="text-align:center">Last Name</th> 
										</tr>
									</thead>
									<tbody>
									
									
									echo $trs; 	
									
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
  </body>
</html>
EOD;
?>

<?php
require_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."Mini-project".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."mpdf".DIRECTORY_SEPARATOR."mpdf".DIRECTORY_SEPARATOR."mpdf.php";

$mpdf = new mPDF('c');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
?>