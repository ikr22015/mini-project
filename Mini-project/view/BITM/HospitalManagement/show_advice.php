<?php
	include_once("../../../vendor/autoload.php");
	
	use App\BITM\RegistrationForm\MedicalHistory;
	use App\BITM\Handler\Utility;
	
	
	$history = new MedicalHistory();
	$info = $history->show_advice($_REQUEST['history_id']);
	//Utility::dd($info);
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../../../resource/Bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- CSS -->
	<link href="../../../resource/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="main">
		<div id="header">
			<figure>
				<a href="index.html"><img src="../../../resource/images/logo.png" width="53" height="65" alt="logo"></a>
				<a href="index.html"><h1>Hospital Registration</h1></a>
			</figure>
			<nav class="nav">
				<ul>
					<li><a class="" href="index.html">Home</a></li>
					<li><a href="list.php">Patient list</a></li>
					<li><a href="history_list.php?id=<?php echo $info->id;?>">Go to History Again</a></li>
				</ul>
			</nav>
		</div>
		<div id="container">
			<div class="SubContainer">
				<button><a href="profile.php?id=<?php echo $info->id?>">Back to Profile</a></button>
				<div id="jumbotron1" class="jumbotron">
					<div class="progile-container">
						<div class="panel panel-default">
						<!-- Default panel contents -->
							<div id="panel-heading" class="panel-heading">Prescription</div>
							<div id="panel-body" class="panel-body">
								<table class="table table-striped">
									<tr>
										<th>ID</th>
										<td><?php echo $info->id?></td>
									</tr>
									<tr>
										<th>Date</th>
										<td><?php echo $info->advice_date?></td>
									</tr>
									<tr>
										<th>Prescribed by</th>
										<td><?php echo $info->doc_name?></td>
									</tr>
									<tr>
										<th>Advice</th>
										<td><span style="color:#9C27B0"><strong><?php echo $info->doc_advice?></strong></span></br>
												Rp.</br>
												Pentobarbitali natrici 3</br>
												Morphiae sulphas 2</br>
												Chlorali hydrati 15</br>
												Saccharum ad 50</br>
												M.f.plv.</br>
												Div. in doses aeq. No XXX (triginta)</br>
												D.S. For sleep: one sachet to be taken at bedtime,</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div id="footer">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>