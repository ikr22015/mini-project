<?php
	include_once("../../../vendor/autoload.php");
	
	use App\BITM\RegistrationForm\PatientInformation;
	use App\BITM\Handler\Utility;
	
	$info = new PatientInformation();
	$profile = $info->profile($_GET['id']);
	
	//Utility::dd($profile);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../../../resource/Bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- CSS -->
	<link href="../../../resource/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="main">
		<div id="header">
			<figure>
				<a href="index.html"><img src="../../../resource/images/logo.png" width="53" height="65" alt="logo"></a>
				<a href="index.html"><h1>Hospital Registration</h1></a>
			</figure>
			<nav class="nav">
				<ul>
					<li><a class="" href="index.html">Home</a></li>
					<li><a href="list.php">Patient list</a></li>
					<li><a href="history_list.php?id=<?php echo $profile->id;?>">Medical History</a></li>
					<li><a href="create_advice.php?id=<?php echo $profile->id;?>">Give Advice</a></li>
				</ul>
			</nav>
		</div>
		<div id="container">
			<div class="SubContainer">
				<div class="RegistrationBox">
					<h2>Create Bill</h2>
					<form action="bill_store.php" method="post">
					<input style="width:300px" type="hidden" class="form-control" name="id" value="<?php echo $profile->id?>">
						
						<div class="from-group">
							<div class="col-md-4 col-md-offset-0">
								<label>Patient Name</label>
								<input style="width:300px" type="text" class="form-control" name="patient-name" required="required" value="<?php echo $profile->first_name?>&nbsp;<?php echo $profile->last_name?>">
							</div>
							<div class="col-md-4 col-md-offset-2">
								<label for="email">Date</label>
								<input style="width:300px" type="date" class="form-control" name="bill-date" required="required" placeholder="dd/mm/yyyy">
							</div>
						</div>
						
						<div class="from-group">
							<div class="col-md-4 col-md-offset-0">
								<label>Description</label>
								<input style="width:300px" type="text" class="form-control" name="description" required="required" placeholder="title">
							</div>
							<div class="col-md-4 col-md-offset-2">
								<label for="email">Amount</label>
								<input style="width:300px" type="number" class="form-control" name="amount" required="required" placeholder="$">
							</div>
						</div>
						
						
						
					
					
						
						
						
						<div class="from-group">
							<div class="col-md-4 col-md-offset-4">
								<button style="width:200px" type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div id="footer">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>