<?php
	include_once("../../../vendor/autoload.php");
	
	use App\BITM\RegistrationForm\BillingInfo;
	use App\BITM\Handler\Utility;
	
	
	$myInvoice = new BillingInfo();
	$invoice = $myInvoice->show_invoice($_REQUEST['bill_id']);

	//Utility::dd($invoice);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../../../resource/Bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- CSS -->
	<link href="../../../resource/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="main">
		<div id="header">
			<figure>
				<a href="index.html"><img src="../../../resource/images/logo.png" width="53" height="65" alt="logo"></a>
				<a href="index.html"><h1>Hospital Registration</h1></a>
			</figure>
			<nav class="nav">
				<ul>
					<li><a class="" href="index.html">Home</a></li>
					<li><a href="list.php">Patient list</a></li>
				</ul>
			</nav>
		</div>
		<div id="container">
			<div class="SubContainer">
			<button type="button"><a href="profile.php?id=<?php echo $invoice->id?>">Back to Profile</a></button>
				<div id="jumbotron1" class="jumbotron">
					<div id="invoice" class="progile-container">
						<div class="panel panel-default">
						<!-- Default panel contents -->
							<div id="panel-body-invioce" class="panel-body">
								<center><h2>Invioce</h2></center>
								<table border="0" id="invoice-date">
									<tr>
										<th>Invioce ID:</th>
										<td><em>000<?php echo $invoice->bill_id?></em></td>
									</tr>
									<tr>
										<th>Patient ID:</th>
										<td><?php echo $invoice->id?></td>
									</tr>
									<tr>
										<th>Date:</th>
										<td><?php echo $invoice->bill_date?></td>
									</tr>
								</table>
								<table>
									<tr style="border:none;">
										<th>Patient Name:</th>
										<td>&nbsp;<?php echo $invoice->patient_name?></td>
									</tr>
								</table>
								<table border="1" class="table">
									<tr id="row-1">
										<th>Item No.</th>
										<th>Description</th>
										<th>Unit Cost</th>
										<th>Quantity</th>
										<th>Cost($)</th>
									</tr>
									<tr style="height:250px">
										<td>1</td>
										<td><?php echo $invoice->description?></td>
										<td>$<?php echo $invoice->amount?></td>
										<td>1</td>
										<td>$<?php echo $invoice->amount?></td>
									</tr>
									<tr>
										<th colspan="4">Subtotal</th>
										<td>$<?php echo $invoice->amount?></td>
									</tr>
									<tr>
										<th colspan="4">Total</th>
										<td><strong>$<?php echo $invoice->amount?></strong></td>
									</tr>
								</table>
							</div>
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
		<div id="footer">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>