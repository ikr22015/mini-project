<?php
	include_once("../../../vendor/autoload.php");
	
	use App\BITM\RegistrationForm\PatientInformation;
	use App\BITM\Handler\Utility;
	
	$info = new PatientInformation();
	$profile = $info->profile($_GET['id']);
	
	//Utility::dd($profile);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../../../resource/Bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- CSS -->
	<link href="../../../resource/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="main">
		<div id="header">
			<figure>
				<a href="index.html"><img src="../../../resource/images/logo.png" width="53" height="65" alt="logo"></a>
				<a href="index.html"><h1>Hospital Registration</h1></a>
			</figure>
			<nav class="nav">
				<ul>
					<li><a class="" href="index.html">Home</a></li>
					<li><a href="list.php">Patient list</a></li>
				</ul>
			</nav>
		</div>
		<div id="container">
			<div class="SubContainer">
				<div class="RegistrationBox">
					<h2>Update Information</h2>
					<form action="update.php" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" value="<?php echo $profile->id?>">
						<div class="from-group">
							<div class="col-md-4 col-md-offset-0">
								<label>First Name</label>
								<input style="width:300px" type="text" class="form-control" name="first-name" required="required" value="<?php echo $profile->first_name?>">
							</div>
							<div class="col-md-4 col-md-offset-2">
								<label for="email">Last Name</label>
								<input style="width:300px" type="text" class="form-control" name="last-name" required="required" value="<?php echo $profile->last_name?>">
							</div>
						</div>
						
						<div class="from-group">
							<div class="col-md-4 col-md-offset-0">
								<label>Address</label>
								<input style="width:300px" type="text" class="form-control" name="address" required="required" value="<?php echo $profile->Address?>">
							</div>
							<div class="col-md-4 col-md-offset-2">
								<label>City</label>
								<select class="form-control" name="city" required="required">
									<option>City where you live</option>
									<option>Dhaka</option>
									<option>Sylhet</option>
									<option>Chittagong</option>
									<option>Rajshahi</option>
									<option>Khulna</option>
									<option>Joshor</option>
									<option>Borishal</option>
									<option>Kumilla</option>
								</select>
							</div>
						</div>
						
						<div class="from-group">
							<div class="col-md-4 col-md-offset-0">
								<label>Date of Birth</label>
								<input style="width:300px" type="date" class="form-control" name="date-of-birth" required="required" value="<?php echo $profile->DOB?>">
							</div>
							
							<div class="col-md-4 col-md-offset-2">
								<label>Sex</label></br>
								<label>Male<input type="radio" name="sex" value="Male" required="required" checked></label>
								<label>Femail<input type="radio" name="sex" required="required" value="Femail"></label>
							</div>
							
						</div>
						
						<div class="from-group">
							<div class="col-md-4 col-md-offset-0">
								<label>Phone</label>
								<input style="width:300px" type="number" class="form-control" name="phone" required="required" value="<?php echo $profile->Phone?>">
							</div>
							<div class="col-md-4 col-md-offset-2">
								<label>National ID number</label>
								<input style="width:300px" type="number" class="form-control" name="national-id" required="required" value="<?php echo $profile->NID?>">
							</div>
						</div>
						
						<div class="from-group">
							<div class="col-md-4 col-md-offset-0">
								<label>Email Address</label>
								<input style="width:300px" type="email" class="form-control" name="email"  required="required" value="<?php echo $profile->email_address?>">
							</div>
							<div class="col-md-4 col-md-offset-2">
								<label>Food Habit</label>
								<select class="form-control" name="food" required="required">
									<option>Vegetarian</option>
									<option>Non-Vegetarian</option>
								</select>
							</div>
						</div>
						
						<div class="from-group">
							<div class="col-md-12 col-md-offset-0">
								<label>Major Diseases</label></br>
								<label><input type="Checkbox" name="diabetes" value="Diabetes">Diabetes</label>
								<label>&nbsp;<input type="Checkbox" name="high-cholesterol" value="High Cholesterol">High Cholesterol</label>
								<label>&nbsp;<input type="Checkbox" name="hypertension" value="Hypertension">Hypertension</label>
								<label>&nbsp;<input type="Checkbox" name="coronary-disease" value="Hypertension">Coronary Artery Disease</label>
								<label>&nbsp;<input type="Checkbox" name="none" value="None of them">None of them</label>
								
							</div>
						</div>
						
						<!--There Was Terms and conditions input content.-->
						
						
						<!--There Was Profile pic input content.-->
						
						
						
						<div class="from-group">
							<div class="col-md-4 col-md-offset-4">
								<button style="width:200px" type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div id="footer">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>