<?php
	include_once("../../../vendor/autoload.php");
	
	use App\BITM\RegistrationForm\PatientInformation;
	use App\BITM\Handler\Utility;
	
	$info = new PatientInformation();
	$profile = $info->profile($_GET['id']);
	
	//Utility::dd($profile);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../../../resource/Bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- CSS -->
	<link href="../../../resource/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="main">
		<div id="header">
			<figure>
				<a href="index.html"><img src="../../../resource/images/logo.png" width="53" height="65" alt="logo"></a>
				<a href="index.html"><h1>Hospital Registration</h1></a>
			</figure>
			<nav class="nav">
				<ul>
					<li><a class="" href="index.html">Home</a></li>
					<li><a href="list.php">Patient list</a></li>
					<li><a href="history_list.php?id=<?php echo $profile->id;?>">Medical History</a></li>
					<li><a href="create_advice.php?id=<?php echo $profile->id;?>">Give Advice</a></li>
					<li><a href="create_bill.php?id=<?php echo $profile->id;?>">Make Bill</a></li>
					<li><a href="bill_list.php?id=<?php echo $profile->id;?>">See Billing List</a></li>
				</ul>
			</nav>
		</div>
		<div id="container">
			<div class="SubContainer">
				<div id="jumbotron1" class="jumbotron">
					<div class="progile-container">
						<h2>Patient Profile</h2>
						
						<figure class="profile-picture">
							<!--<img src="../../../resource/images/profile-pic.jpg" width="200" height="200" alt="ProfilePicture" class="img-thumbnail">-->
							<?php echo "<img src='images/$profile->profile_picture' width='200' height='200' alt='profile-picture' class='img-thumbnail';>"?>
						</figure>
						
						<h3><?php echo $profile->first_name;?>&nbsp;<?php echo $profile->last_name;?></h3>
						
						<div id="id">
							<h4>ID:&nbsp;&nbsp;<?php echo $profile->id;?></h4>
						</div>
						
						<div id="birthdate">
							<h4>Date of Birth:</h4><p><?php echo $profile->DOB;?></p>
						</div>
						
						<address>
							<h4>Address:&nbsp;</h4><p><?php echo $profile->Address;?></p>
						</address>
						
						<div id="city">
							<h4>City:</h4><p><?php echo $profile->City;?></p>
						</div>
						
						<div id="sex">
							<h4>Sex:</h4><p><?php echo $profile->SEX;?></p>
						</div>
						
						<div id="phone">
							<h4>Phone:</h4><p><?php echo $profile->Phone;?></p>
						</div>
						
						<div id="id-num">
							<h4>National ID number:</h4><p><?php echo $profile->NID;?></p>
						</div>
						
						<div id="email">
							<h4>Email Address:</h4><p><?php echo $profile->email_address;?></p>
						</div>
						
						<div id="habit">
							<h4>Food Habit:</h4><p><?php echo $profile->food_habit;?></p>
						</div>
						
						<div id="diseases">
							<h4>Major Diseases:</h4>
							<ul>
								<li><?php echo $profile->Diabetes;?></li>
								<li><?php echo $profile->Cholesterol;?></li>
								<li><?php echo $profile->Hypertension;?></li>
								<li><?php echo $profile->Coronary;?></li>
								<li><?php echo $profile->None;?></li>
							</ul>
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
		<div id="footer">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>