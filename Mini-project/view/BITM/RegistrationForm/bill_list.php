<?php
	include_once("../../../vendor/autoload.php");
	
	use App\BITM\RegistrationForm\BillingInfo;
	use App\BITM\RegistrationForm\PatientInformation;
	use App\BITM\Handler\Utility;
	
	
	$info = new PatientInformation();
	$profile = $info->profile($_REQUEST['id']);
	
	
	$myBill = new BillingInfo();
	$bill = $myBill->bill_list($_REQUEST['id']);

	//Utility::dd($bill);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../../../resource/Bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- CSS -->
	<link href="../../../resource/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="main">
		<div id="header">
			<figure>
				<a href="index.html"><img src="../../../resource/images/logo.png" width="53" height="65" alt="logo"></a>
				<a href="index.html"><h1>Hospital Registration</h1></a>
			</figure>
			<nav class="nav">
				<ul>
					<li><a class="" href="index.html">Home</a></li>
					<li><a href="list.php">Bill list</a></li>
					<li><a href="history_list.php?id=<?php echo $profile->id;?>">Medical History</a></li>
					<li><a href="create_advice.php?id=<?php echo $profile->id;?>">Give Advice</a></li>
				</ul>
			</nav>
		</div>
		<div id="container">
			<div class="SubContainer">
				<div id="jumbotron" class="jumbotron">
					<div class="container">
						<div class="booklist-sub-container">
							<div class="bill-list-nav">
								<ul>
									<li>
										<form action="filter.php" method="get">
											<input type="number" name="filter" placeholder="Search by ID...">
											<button type="submit" name="" value="">Search</button>
										</form>
									</li>
									<li></li>
									<li></li>
									<li><a href="profile.php?id=<?php echo $profile->id?>">Back to Profile</a></li>
								</ul>
							</div>
							<div id="profile-sum-org-panel-success" class="panel panel-primary">
								<div id="profile-sum-org-panel-heading" class="panel-heading">
									<p style="margin: 0; text-align:center;">Billing History</p>
								</div>
								<table class="table table-bordered" border="1">
									<thead>
										<tr>
											<th>Bill ID</th>
											<th>ID</th>
											<th>Bill Date</th> 
											<th>Name</th> 
											<th>Action</th> 
											
										</tr>
									</thead>
									<tbody>
									<?php
									foreach($bill as $myBill){
									?>
										<tr>
											<td><?php echo $myBill->bill_id?></td>
											<td><?php echo $myBill->id?></td> 
											<td><?php echo $myBill->bill_date?></td>  
											<td><?php echo $myBill->patient_name?></td>  
											<td>
												<input type="hidden" name="history_id" value="<?php echo $history->history_id?>">
												<button type="button" name="button" value=""><a href="invoice.php?bill_id=<?php echo $myBill->bill_id?>">View Invoice</a></button>
											</td>
										</tr>
									<?php
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="footer">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>