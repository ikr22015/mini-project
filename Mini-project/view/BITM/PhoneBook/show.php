<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Mini-project'.DIRECTORY_SEPARATOR.'view'.DIRECTORY_SEPARATOR.'startup.php');
use \App\BITM\PhoneBook\PhoneBook;
use App\BITM\Handler\Utility;

$phone_obj=new PhoneBook();
$phone_objs=$phone_obj->show($_GET['id']);

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mini Project </title>
	

 <link href="../../../resource/Bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<link href="../../../resource/css/t_styles.css" rel="stylesheet"/>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
	#message{
       background-color:#66A523;
		}
        </style>
	</head>
	<body class="flaticon bg-login" style="overflow: visible;">
		<div class="container">
			<!-- header start-->
		<header><br>
			<div class="row header">
				<div class="col-md-4">
				</div>
				<div class="col-md-4">
					<h1>Phone Book</h1>
				</div> 
				<div class="col-md-4">
					
				</div>  				
			</div>
		</header>
			<!-- header End-->
			
			<!-- middle body start-->			
			<section style="display: block;" class="gr_box login" id="gr_loginbox">		
				<div class="box">
						<a href="../../../index.html" button type="button" class="btn btn-primary">All Project</button></a>
						<a href="create.php" button type="button" class="btn btn-primary">Add New Contact</button></a>
						<a href="index.php" button type="button" class="btn btn-primary">Contact List</button></a>
					<p class="separator"><span>&nbsp;</span></p>
				<div class="row header">
					<div class="col-md-4">
					</div>
					<div class="col-md-4 text-center">
						<h6><b>ID :</b>&nbsp;<?php echo $phone_objs['id'];?></h6>
					</div> 
					<div class="col-md-4">
						
					</div> <br>
				</div>	
					
				<div class="row header">
					<div class="col-md-4">
					</div>
					<div class="col-md-4 text-center">
						<?php echo "<img src='images/$phone_objs[picture]' width='80' height='80' alt='picture here'>"?>
					</div> 
					<div class="col-md-4">
						
					</div><br>
				</div>
				
				<div class="row header">
					<div class="col-md-4">
					</div>
					<div class="col-md-4 text-center">
						<h6><b>Name :</b>&nbsp;<?php echo $phone_objs['name'];?></h6>
					</div> 
					<div class="col-md-4">
						
					</div> <br>
				</div>
				<div class="row header">
					<div class="col-md-4">
					</div>
					<div class="col-md-4 text-center">
						<h6><b>Phone Number :</b>&nbsp;<?php echo $phone_objs['pho_number'];?></h6>
					</div> 
					<div class="col-md-4">
			
					</div> <br>
				</div>
				<div class="row header">
					<div class="col-md-4">
					</div>
					<div class="col-md-4 text-center">
						<h6><b>Mobile Number :</b>&nbsp;<?php echo $phone_objs['mob_number'];?></h6>
					</div> 
					<div class="col-md-4">
						
					</div> <br>
				</div>
				<div class="row header">
					<div class="col-md-4">
					</div>
					<div class="col-md-4 text-center">
						<h6><b>Email :</b>&nbsp;<?php echo $phone_objs['email'];?></h6>
					</div> 
					<div class="col-md-4">
						
					</div> 
				</div>
				
			</section>		
				
			<!-- middle body End-->

				
			<!-- Footer start-->

			<div class="col-md-4">
			</div>
			
			<div class="col-md-8">
				<p>Copyright @ The Code Warriors</p>
			</div>
			<!-- Footer End-->

		</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins)-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
   <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>
       
	
	
  </body>
</html