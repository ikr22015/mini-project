<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Mini-project'.DIRECTORY_SEPARATOR.'view'.DIRECTORY_SEPARATOR.'startup.php');
use \App\BITM\PhoneBook\PhoneBook;
use App\BITM\Handler\Utility;

$phone_obj=new PhoneBook();
$phone_objs=$phone_obj->show($_GET['id']);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mini Project </title>
	

 <link href="../../../resource/Bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
 <link href="../../../resource/css/t_styles.css" rel="stylesheet"/>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	
	</head>
	<body class="flaticon bg-login" style="overflow: visible;">
		<div class="container">
			<!-- header start-->
		<header><br>
			<div class="row header">
				<div class="col-md-4">
				</div>
				<div class="col-md-8">
					<!--<img class="img-responsive" src="image/banner_1.png"/>-->
					<h1>Phone Book</h1>
				</div>  
			</div>
		</header>
			<!-- header End-->
			
			<!-- middle body start-->
			<section style="display: block;" class="gr_box login" id="gr_loginbox">		
				<div class="box">
	
						<a href="../../../index.html" button type="button" class="btn btn-primary">All Project</button></a>
						<a href="create.php" button type="button" class="btn btn-primary">Add New Contact</button></a>
						<a href="index.php" button type="button" class="btn btn-primary">Contact List</button></a>
						
					<p class="separator"><span>&nbsp;</span></p>

					<form action="update.php" method="post" enctype="multipart/form-data">
						<div class="boxProfile userProfile">
							<div class="boxProfile-fields profile-data form">
								<div class="profile-block">
									<div class="profile-col">
										<span>
											<input type="hidden" name="id" value="<?php echo $phone_objs['id'];?>" size="30"/> 											
										</span>
										<span>
											<label for="picture">Select image to upload:</label> 
											<input type="FILE" name="avatar" value="<?php echo "<img src='images/$phone_objs[picture]' width='80' height='80' alt='picture here'>"?>">
										</span></br>
										<span >
											<label for="name">Name</label>
											<input type="text" tabindex="1" value="<?php echo $phone_objs['name'];?>" id="name" name="name" required="required" autofocus="">											
										</span>
										<span >
											<label for="mobilenumber">Mobile Number</label>
											<input type="number" tabindex="3" value="<?php echo $phone_objs['mob_number']?>" id="mobile" name="mobile" required="required">											
										</span>
										<span >
											<label for="phonenumber">Phone Number</label>
											<input type="number" tabindex="2" value="<?php echo $phone_objs['pho_number'];?>" id="phone" name="phone" >																															
										</span>
										<span>
											<label for="email">Email</label>
											<input type="text" tabindex="4" value="<?php echo $phone_objs['email']?>" id="email" name="email">											
										</span>
										

									</div>
								</div>
								
								<div>
									<button id="updateProfileBtn" tabindex="5" class="btn btn-primary" type="submit">Save Contact</button>
									<button id="updateProfileBtn" tabindex="6" class="btn btn-primary" type="reset">Reset</button>

								</div>
							</div>
						</div>
					</form>
				</div>
			</section>		
			
			
			

				
			<!-- middle body End-->

				
			<!-- Footer start-->

			<div class="col-md-4">
			</div>
			
			<div class="col-md-8">
				<p>Copyright @ The Code Warriors</p>
			</div>
			<!-- Footer End-->

		</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins)
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    Include all compiled plugins (below), or include individual files as needed 
    <script src="js/bootstrap.min.js"></script>-->
  </body>
</html>