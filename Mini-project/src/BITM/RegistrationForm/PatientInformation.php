<?php
	namespace App\BITM\RegistrationForm;
	
	use App\BITM\Handler\Utility;
	
	class PatientInformation{
		
		public $id = "";
		public $first_name = "";
		public $last_name = "";
		public $Address = "";
		public $City = "";
		public $DOB = "";
		public $SEX = "";
		public $Phone = "";
		public $NID = "";
		public $email_address = "";
		public $food_habit = "";
		public $Diabetes = "";
		public $Cholesterol = "";
		public $Hypertension = "";
		public $Coronary = "";
		public $None = "";
		public $Terms = "";
		public $profile_picture = "";
		public $temp = "";
		
		
		public function __construct($data = false){
			
			if(is_array($data) && array_key_exists('id', $data) && !empty($data['id'])){
				$this->id = $data["id"];
			}
			
			@$this->first_name = $data["first-name"];
			@$this->last_name = $data["last-name"];
			@$this->Address = $data["address"];
			@$this->City = $data["city"];
			@$this->DOB = $data["date-of-birth"];
			@$this->SEX = $data["sex"];
			@$this->Phone = $data["phone"];
			@$this->NID = $data["national-id"];
			@$this->email_address = $data["email"];
			@$this->food_habit = $data["food"];
			@$this->Diabetes = $data["diabetes"];
			@$this->Cholesterol = $data["high-cholesterol"];
			@$this->Hypertension = $data["hypertension"];
			@$this->Coronary = $data["coronary-disease"];
			@$this->None = $data["none"];
			@$this->Terms = $data["tarms"];
			
			@$this->profile_picture = $_FILES["profile-pic"]["name"];
			@$this->temp = $_FILES["profile-pic"]["tmp_name"];
		}
		
		public function list_of_data(){
			
			$allinfo = array();
			
			$connection = mysql_connect("localhost","root","") or die("Database cannot connected.");
			
			$link = mysql_select_db("db_hospital_registration") or die("Database name is not selected perfectly. Please cheack it.");
			
			$query = "SELECT * FROM `patient_information`";
			
			$result = mysql_query($query);
			
			while($row = mysql_fetch_object($result)){
				$allinfo[] = $row;
			}
			
			return $allinfo;
		}
		
		
		
		public function store(){
			
			$connection = mysql_connect("localhost","root","") or die("Database cannot connected.");
			
			$link = mysql_select_db("db_hospital_registration") or die("Database name is not selected perfectly. Please cheack it.");
			
			move_uploaded_file($this->temp, "images/$this->profile_picture");
			
			$query = "INSERT INTO `db_hospital_registration`.`patient_information` ( `first_name`, `last_name`, `Address`, `City`, `DOB`, `SEX`, `Phone`, `NID`, `email_address`, `food_habit`, `Diabetes`, `Cholesterol`, `Hypertension`, `Coronary`, `None`, `Terms`, `profile_picture`) VALUES ( '".$this->first_name."', '".$this->last_name."', '".$this->Address."', '".$this->City."', '".$this->DOB."', '".$this->SEX."', '".$this->Phone."', '".$this->NID."', '".$this->email_address."', '".$this->food_habit."', '".$this->Diabetes."', '".$this->Cholesterol."', '".$this->Hypertension."', '".$this->Coronary."', '".$this->None."', '".$this->Terms."', '".$this->profile_picture."')";
			
			
			$result = mysql_query($query);
			
			if($result){
					Utility::message("The patient profile has been added successfully.");
				}else{
					Utility::message("There is something error. Please try again later.");
				}
			
			Utility::redirect("list.php");
			
			
		}
		
		
		public function profile($id = null){
			
			
			$connection = mysql_connect("localhost","root","") or die("Database cannot connected.");
			
			$link = mysql_select_db("db_hospital_registration") or die("Database name is not selected perfectly. Please cheack it.");
			
			$query = "SELECT * FROM `patient_information` WHERE `patient_information`.`id` = ".$id;
					
			
			$result = mysql_query($query);
			
			$row = mysql_fetch_object($result);
			
			return $row;
			
		}
		
		public function delete_profile($id = false){
			/*if(is_null($id)){
				Utility::message("ID is not available.");
				return Utility::redirect("list.php");
			}*/
			
			$connection = mysql_connect("localhost","root","") or die("Database cannot connected.");
			
			$link = mysql_select_db("db_hospital_registration") or die("Database name is not selected perfectly. Please cheack it.");
			
			$query = "DELETE FROM `db_hospital_registration`.`patient_information` WHERE `patient_information`.`id` = ".$id;
			
			$result = mysql_query($query);
			
			if($result){
					Utility::message("The patient profile has been deleted successfully.");
				}else{
					Utility::message("There is something error. Please try again later.");
				}
			
			Utility::redirect("list.php");
			
			//Utility::dd($query);
			
		}
		
		
		
		
		public function update(){
			
			$connection = mysql_connect("localhost","root","") or die("Database cannot connected.");
			
			$link = mysql_select_db("db_hospital_registration") or die("Database name is not selected perfectly. Please cheack it.");
			
			move_uploaded_file($this->temp, "images/$this->profile_picture");
			
			$query = "UPDATE `db_hospital_registration`.`patient_information` SET `first_name` = '".$this->first_name."', `last_name` = '".$this->last_name."', `Address` = '".$this->Address."', `City` = '".$this->City."', `DOB` = '".$this->DOB."', `SEX` = '".$this->SEX."', `Phone` = '".$this->Phone."', `NID` = '".$this->NID."', `email_address` = '".$this->email_address."', `food_habit` = '".$this->food_habit."', `Diabetes` = '".$this->Diabetes."', `Cholesterol` = '".$this->Cholesterol."', `Hypertension` = '".$this->Hypertension."', `Coronary` = '".$this->Coronary."', `None` = '".$this->None."', `Terms` = '".$this->Terms."', `profile_picture` = '".$this->profile_picture."' WHERE `patient_information`.`id` = ".$this->id;
			
			
			
			$result = mysql_query($query);
			
			if($result){
					Utility::message("The patient profile has been updated successfully.");
				}else{
					Utility::message("There is something error. Please try again later.");
				}
			
			Utility::redirect("list.php");
			
			
		}
		
		
		
		public function filter(){
			$allinfo = array();
			
			$this->id = $_POST['filter'];
			
			$connection = mysql_connect("localhost","root","") or die("Database cannot connected.");
			
			$link = mysql_select_db("db_hospital_registration") or die("Database name is not selected perfectly. Please cheack it.");
			
			$query = "SELECT * FROM `patient_information` WHERE `id` LIKE '".$this->id."'";
			
			
			
			$result = mysql_query($query);
			
			//Utility::dd($result);
			
			while($row = mysql_fetch_object($result)){
				$allinfo[] = $row;
			}
			
			return $allinfo;
		}
	}
?>