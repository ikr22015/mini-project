<?php
	namespace App\BITM\Handler;
	if (session_id() === "") {
    session_start();
}
	
	class Utility{
		
		static public function d($peram = false){
			echo "<pre>";
				var_dump($peram);
			echo "</pre>";
		}

		static public function dd($peram = false){
			self::d($peram);
			die();
		}
		
		
		static public function redirect($url = null){
			header("Location:".$url);
		}
		
		
		static public function message($message = null){
			if(is_null($message)){
				$_message = self::getMessage();
				return $_message;
			}else{
				self::setMessage($message);
			}
		}


		static private function getMessage(){
			$_message = $_SESSION['message'];
			$_SESSION['message'] = "";
			return $_message;
		}

		static private function setMessage($message){
			$_SESSION['message'] = $message;
		}
	}

?>