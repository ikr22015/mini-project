-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2016 at 12:03 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_hospital_registration`
--

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE IF NOT EXISTS `bill` (
  `bill_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `patient_name` varchar(255) NOT NULL,
  `bill_date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `amount` int(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill`
--

INSERT INTO `bill` (`bill_id`, `id`, `patient_name`, `bill_date`, `description`, `amount`) VALUES
(22, 27, 'Jasmin AktherÂ Chowdhury', '2016-01-16', 'Advice', 100),
(23, 27, 'Jasmin AktherÂ Chowdhury', '2016-01-22', 'Medicine', 500),
(24, 28, 'Maruf Â Ahmed', '2016-01-20', 'Medicine', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `patient_history`
--

CREATE TABLE IF NOT EXISTS `patient_history` (
  `history_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `doc_name` varchar(255) NOT NULL,
  `advice_date` date NOT NULL,
  `doc_advice` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_history`
--

INSERT INTO `patient_history` (`history_id`, `id`, `doc_name`, `advice_date`, `doc_advice`) VALUES
(32, 27, 'Dr. Imran Kabir', '2016-01-13', '<p>Protidin Shokal bela utben. Khawar por shudhu ghumaben na.</p>'),
(33, 27, 'Dr. Khaled mohsin', '2016-01-16', '<p>Need Sound sleep.</p>'),
(34, 28, 'Dr . A Malik', '2016-01-18', '<p>Drink fresh watar.</p>'),
(35, 28, 'Dr. Amin', '2016-01-14', '<p>Eat egg.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `patient_information`
--

CREATE TABLE IF NOT EXISTS `patient_information` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `City` varchar(255) NOT NULL,
  `DOB` date NOT NULL,
  `SEX` varchar(255) NOT NULL,
  `Phone` int(255) NOT NULL,
  `NID` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `food_habit` varchar(255) NOT NULL,
  `Diabetes` varchar(255) NOT NULL,
  `Cholesterol` varchar(255) NOT NULL,
  `Hypertension` varchar(255) NOT NULL,
  `Coronary` varchar(255) NOT NULL,
  `None` varchar(255) NOT NULL,
  `Terms` varchar(255) NOT NULL,
  `profile_picture` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_information`
--

INSERT INTO `patient_information` (`id`, `first_name`, `last_name`, `Address`, `City`, `DOB`, `SEX`, `Phone`, `NID`, `email_address`, `food_habit`, `Diabetes`, `Cholesterol`, `Hypertension`, `Coronary`, `None`, `Terms`, `profile_picture`) VALUES
(27, 'Jasmin Akther', 'Chowdhury', 'House: 35, Road: 37, Block-C, Shajalal Uposhohor. Sylhet.', 'Sylhet', '2016-01-21', 'Femail', 2147483647, '7845123265', 'jasmin@yahoo.com', 'Non-Vegetarian', '', '', '', '', 'None of them', 'Agree with terms', 'Untitled-1.jpg'),
(28, 'Maruf ', 'Ahmed', 'House: 35, Road: 37, Block-C, Shajalal Uposhohor. Sylhet.', 'Dhaka', '2016-01-02', 'Male', 2147483647, '54656556556', 'dsafdsa@mail.com', 'Vegetarian', 'Diabetes', 'High Cholesterol', 'Hypertension', 'Hypertension', '', 'Agree with terms', 'Untitled-12.jpg'),
(29, 'Ahmed', 'Kamal', 'House: 78, Road: 79, Block-A, Shajalal Uposhohor. Sylhet.', 'Borishal', '2016-01-23', 'Male', 2147483647, '787454545454', 'dsafdsa@mail.com', 'Vegetarian', 'Diabetes', 'High Cholesterol', 'Hypertension', 'Hypertension', '', 'Agree with terms', 'Untitled-11 copy.jpg'),
(30, 'Korimunnesa', 'Khatun', 'House: 78, Road: 79, Block-A, Shajalal Uposhohor. Sylhet.', 'Rajshahi', '2016-01-09', 'Femail', 2147483647, '878754542211', 'kamranromim@gmail.com', 'Non-Vegetarian', 'Diabetes', '', '', '', 'None of them', 'Agree with terms', 'Untitled-5.jpg'),
(32, 'Jamil U ', 'Reza', 'House: 35, Road: 37, Block-C, Shajalal Uposhohor. Sylhet.', 'Rajshahi', '2016-01-09', 'Male', 2147483647, '45454545454', 'dsafdsa@mail.com', 'Vegetarian', '', 'High Cholesterol', 'Hypertension', 'Hypertension', '', 'Agree with terms', 'Untitled-9 copy.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `phonebooks`
--

CREATE TABLE IF NOT EXISTS `phonebooks` (
  `id` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `name` varchar(16) NOT NULL,
  `pho_number` int(16) NOT NULL,
  `mob_number` int(16) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `phonebooks`
--

INSERT INTO `phonebooks` (`id`, `picture`, `name`, `pho_number`, `mob_number`, `email`) VALUES
(1, 'Untitled-1.jpg', 'Sadia Rahman', 27568954, 1816364709, 'sadia@gmail.com'),
(2, 'Untitled-2 copy.jpg', 'Tahmina Aktar', 27568954, 1816364705, 'tahmina27185@gmail.com'),
(3, 'Untitled-7 copy.jpg', 'Tahmina Aktar', 27589569, 1813698156, 'meenaeity@gmail.com'),
(4, 'Untitled-1.jpg', 'Tahmina Aktar', 27568954, 1816364703, 'tahmina27185@gmail.com'),
(6, 'Untitled-8 copy.jpg', 'Md. Shamim Ahmed', 27568954, 1813698156, 'tahmina27185@gmail.com'),
(8, 'Untitled-9 copy.jpg', 'Shamsuddin', 27568954, 1816364705, 'tahshamim@gmail.com'),
(9, 'Untitled-3.jpg', 'Md. Shamim', 27568954, 1813698156, 'tahshamim@gmail.com'),
(10, 'Untitled-2 copy.jpg', 'Mrs.Tahmina', 27589569, 1816364703, 'meenaeity@gmail.com'),
(11, 'Untitled-7 copy.jpg', 'Sisty', 2758964, 1813698156, 'sadia@gmail.com'),
(12, 'Untitled-1.jpg', 'Samira', 27568954, 1816364705, 'samira@gmail.com'),
(13, 'Untitled-2 copy.jpg', 'Sarmin', 25897563, 17895639, 'tahmina@gmail.com'),
(14, 'Untitled-6 copy.jpg', 'Bill clinton ', 269846, 1726725896, ''),
(15, 'Untitled-7 copy.jpg', 'Mrs.Bithy ', 2369857, 1762616860, 'bithy@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`bill_id`), ADD KEY `id` (`id`);

--
-- Indexes for table `patient_history`
--
ALTER TABLE `patient_history`
  ADD PRIMARY KEY (`history_id`), ADD KEY `id` (`id`);

--
-- Indexes for table `patient_information`
--
ALTER TABLE `patient_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phonebooks`
--
ALTER TABLE `phonebooks`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bill`
--
ALTER TABLE `bill`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `patient_history`
--
ALTER TABLE `patient_history`
  MODIFY `history_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `patient_information`
--
ALTER TABLE `patient_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `phonebooks`
--
ALTER TABLE `phonebooks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
